# Singleton 
## Lazy Instantiation 
Pendekatan dengan metode ini *instance* dari *class* hanya dibuat saat dibutuhkan atau saat dipanggil saja. Ada kemungkinan bahwa tidak akan ada satu *instance* pun yang dibuat hingga aplikasi dimatikan. Teknik ini dapat menghemat penggunaan memori, tetapi dapat mengurangi performa dari aplikasi.
## Eager Instantiation 
Lain halnya dengan metode ini, di mana *instance* pasti dibuat terlepas *instance* ini dibutuhkan atau tidak. Secara performa, pendekatan ini dapat menghemat waktu eksekusi program karena *instance* sudah siap dari awal. Namun, akan mubazir memori apabila terlalu banyak *instance* yang tidak digunakan. 