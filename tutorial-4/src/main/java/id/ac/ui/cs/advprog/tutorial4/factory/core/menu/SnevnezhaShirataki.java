package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.SnevnezhaShiratakiFactory;

public class SnevnezhaShirataki extends Menu {
    public SnevnezhaShirataki(String name){
        super(name, new SnevnezhaShiratakiFactory());
    }
}