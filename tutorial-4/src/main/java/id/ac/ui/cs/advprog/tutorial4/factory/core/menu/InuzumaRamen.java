package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.InuzumaRamenFactory;

public class InuzumaRamen extends Menu {

    public InuzumaRamen(String name){
        super(name, new InuzumaRamenFactory());
    }
}