package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.MondoUdonFactory;

public class MondoUdon extends Menu {

    public MondoUdon(String name){
        super(name, new MondoUdonFactory());
    }
}