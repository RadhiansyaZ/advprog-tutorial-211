package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuServiceTest {
    MenuService menuService;

    @BeforeEach
    public void setUp() {
        menuService = new MenuServiceImpl();
    }

    @Test
    public void testCreateMenu() {
        Menu newMenuSoba = menuService.createMenu("Yaki Soba", "Soba");
        assertTrue(menuService.getMenus().contains(newMenuSoba));

        Menu newMenuRamen = menuService.createMenu("Char Siu Ramen", "Ramen");
        assertTrue(menuService.getMenus().contains(newMenuRamen));

        Menu newMenuUdon = menuService.createMenu("Niku Udon", "Udon");
        assertTrue(menuService.getMenus().contains(newMenuUdon));
    }

}
