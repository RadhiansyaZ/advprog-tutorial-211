package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MeatTest {
    private static List<Meat> meatList;

    @BeforeAll
    public static void setUp() {
        meatList = new ArrayList<>();
        meatList.add(new Beef());
        meatList.add(new Chicken());
        meatList.add(new Fish());
        meatList.add(new Pork());
    }

    @Test
    public void testAllMeatsAreNotAbstract() {
        for(Meat concrete : meatList) {
            Class<?> concreteClass = concrete.getClass();

            assertFalse(Modifier.isAbstract(concreteClass.getModifiers()));
        }
    }

    @Test
    public void testAllMeatsImplementsMeat() {
        for(Meat concrete : meatList) {
            Class<?> concreteClass = concrete.getClass();

            assertTrue(Meat.class.isAssignableFrom(concreteClass));
        }
    }

    @Test
    public void testAllMeatsHaveDifferentDescriptions() {
        HashSet<String> meatDescriptions = new HashSet<>();

        for(Meat concrete : meatList) {
            String desc = concrete.getDescription();

            meatDescriptions.add(desc);
            assertTrue(meatDescriptions.contains(desc));
        }
    }
}