package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiFactoryTest {

    private static SnevnezhaShiratakiFactory snevnezhaShiratakiFactory;

    @BeforeAll
    public static void setUp() {
        snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
    }

    @Test
    public void testSnevnezhaShiratakiFactoryIsConcrete() {
        assertFalse(Modifier.isAbstract(SnevnezhaShiratakiFactory.class.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryImplementsIngredientsFactory() {
        assertTrue(IngredientsFactory.class.isAssignableFrom(SnevnezhaShiratakiFactory.class));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryReturnsCorrectIngredients() {
        assertTrue(snevnezhaShiratakiFactory.getFlavor() instanceof Umami);
        assertTrue(snevnezhaShiratakiFactory.getMeat() instanceof Fish);
        assertTrue(snevnezhaShiratakiFactory.getNoodle() instanceof Shirataki);
        assertTrue(snevnezhaShiratakiFactory.getTopping() instanceof Flower);
    }

}
