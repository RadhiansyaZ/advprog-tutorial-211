package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaFactoryTest {

    private static LiyuanSobaFactory liyuanSobaFactory;

    @BeforeAll
    public static void setUp() {
        liyuanSobaFactory = new LiyuanSobaFactory();
    }

    @Test
    public void testLiyuanSobaFactoryIsConcrete() {
        assertFalse(Modifier.isAbstract(LiyuanSobaFactory.class.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryImplementsIngredientsFactory() {
        assertTrue(IngredientsFactory.class.isAssignableFrom(LiyuanSobaFactory.class));
    }

    @Test
    public void testLiyuanSobaFactoryReturnsCorrectIngredients() {
        assertTrue(liyuanSobaFactory.getFlavor() instanceof Sweet);
        assertTrue(liyuanSobaFactory.getMeat() instanceof Beef);
        assertTrue(liyuanSobaFactory.getNoodle() instanceof Soba);
        assertTrue(liyuanSobaFactory.getTopping() instanceof Mushroom);
    }

}
