package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderServiceTest {
    private static OrderServiceImpl orderService;
    private static OrderFood orderFood;
    private static OrderDrink orderDrink;


    @BeforeAll
    static void setUp() throws Exception {
        orderService = new OrderServiceImpl();
        orderFood = OrderFood.getInstance();
        orderDrink = OrderDrink.getInstance();
    }

    @Test
    public void testOrderServiceTestOrderFood() {
        String foodName = "Some tasty food";
        orderService.orderAFood(foodName);

        String result = orderService.getFood().getFood();

        assertEquals(foodName, result);
    }

    @Test
    public void testOrderServiceTestOrderDrink() {
        String drinkName = "Some tasty drink";
        orderService.orderADrink(drinkName);

        String result = orderService.getDrink().getDrink();

        assertEquals(drinkName, result);
    }

    @Test
    public void testOrderFoodOnlyOneInstance() {
        assertEquals(orderFood, orderService.getFood());
    }
    @Test
    public void testOrderDrinkOnlyOneInstance() {
        assertEquals(orderDrink, orderService.getDrink());
    }
}
