package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class SnevnezhaShiratakiTest {
    private static String snevnezhaShiratakiName;
    private static SnevnezhaShirataki snevnezhaShirataki;

    @BeforeAll
    public static void setUp() {
        snevnezhaShiratakiName = "Tartaglia SnevnezhaShirataki";
        snevnezhaShirataki = new SnevnezhaShirataki(snevnezhaShiratakiName);
    }

    @Test
    public void testSnevnezhaShiratakiHasCorrectIngredients() {
        assertTrue(snevnezhaShirataki.getNoodle() instanceof Shirataki);
        assertTrue(snevnezhaShirataki.getMeat() instanceof Fish);
        assertTrue(snevnezhaShirataki.getTopping() instanceof Flower);
        assertTrue(snevnezhaShirataki.getFlavor() instanceof Umami);
    }

    @Test
    public void testSnevnezhaShiratakiHasCorrectName() {
        assertEquals(snevnezhaShiratakiName, snevnezhaShirataki.getName());
    }
}
