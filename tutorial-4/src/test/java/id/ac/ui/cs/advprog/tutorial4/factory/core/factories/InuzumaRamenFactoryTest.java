package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenFactoryTest {

    private static InuzumaRamenFactory inuzumaRamenFactory;

    @BeforeAll
    public static void setUp() {
        inuzumaRamenFactory = new InuzumaRamenFactory();
    }

    @Test
    public void testInuzumaRamenFactoryIsConcrete() {
        assertFalse(Modifier.isAbstract(InuzumaRamenFactory.class.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryImplementsIngredientsFactory() {
        assertTrue(IngredientsFactory.class.isAssignableFrom(InuzumaRamenFactory.class));
    }

    @Test
    public void testInuzumaRamenFactoryReturnsCorrectIngredients() {
        assertTrue(inuzumaRamenFactory.getFlavor() instanceof Spicy);
        assertTrue(inuzumaRamenFactory.getMeat() instanceof Pork);
        assertTrue(inuzumaRamenFactory.getNoodle() instanceof Ramen);
        assertTrue(inuzumaRamenFactory.getTopping() instanceof BoiledEgg);
    }

}
