package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NoodleTest {
    private static List<Noodle> noodleList;

    @BeforeAll
    public static void setUp() {
        noodleList = new ArrayList<>();
        noodleList.add(new Ramen());
        noodleList.add(new Shirataki());
        noodleList.add(new Soba());
        noodleList.add(new Udon());
    }

    @Test
    public void testAllNoodlesAreNotAbstract() {
        for(Noodle concrete : noodleList) {
            Class<?> concreteClass = concrete.getClass();

            assertFalse(Modifier.isAbstract(concreteClass.getModifiers()));
        }
    }

    @Test
    public void testAllNoodlesImplementsNoodle() {
        for(Noodle concrete : noodleList) {
            Class<?> concreteClass = concrete.getClass();

            assertTrue(Noodle.class.isAssignableFrom(concreteClass));
        }
    }

    @Test
    public void testAllNoodlesHaveDifferentDescriptions() {
        HashSet<String> noodleDescriptions = new HashSet<>();

        for(Noodle concrete : noodleList) {
            String desc = concrete.getDescription();

            noodleDescriptions.add(desc);
            assertTrue(noodleDescriptions.contains(desc));
        }
    }
}