package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class InuzumaRamenTest {
    private static String ramenName;
    private static InuzumaRamen inuzumaRamen;

    @BeforeAll
    public static void setUp() {
        ramenName = "Ichiraku Ramen";
        inuzumaRamen = new InuzumaRamen(ramenName);
    }

    @Test
    public void testRamenHasCorrectIngredients() {
        assertTrue(inuzumaRamen.getNoodle() instanceof Ramen);
        assertTrue(inuzumaRamen.getMeat() instanceof Pork);
        assertTrue(inuzumaRamen.getTopping() instanceof BoiledEgg);
        assertTrue(inuzumaRamen.getFlavor() instanceof Spicy);
    }

    @Test
    public void testRamenHasCorrectName() {
        assertEquals(ramenName, inuzumaRamen.getName());
    }
}
