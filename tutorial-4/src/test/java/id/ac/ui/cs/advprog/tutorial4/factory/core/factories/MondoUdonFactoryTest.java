package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonFactoryTest {

    private static MondoUdonFactory mondoUdonFactory;

    @BeforeAll
    public static void setUp() {
        mondoUdonFactory = new MondoUdonFactory();
    }

    @Test
    public void testMondoUdonFactoryIsConcrete() {
        assertFalse(Modifier.isAbstract(MondoUdonFactory.class.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryImplementsIngredientsFactory() {
        assertTrue(IngredientsFactory.class.isAssignableFrom(MondoUdonFactory.class));
    }

    @Test
    public void testMondoUdonFactoryReturnsCorrectIngredients() {
        assertTrue(mondoUdonFactory.getFlavor() instanceof Salty);
        assertTrue(mondoUdonFactory.getMeat() instanceof Chicken);
        assertTrue(mondoUdonFactory.getNoodle() instanceof Udon);
        assertTrue(mondoUdonFactory.getTopping() instanceof Cheese);
    }

}
