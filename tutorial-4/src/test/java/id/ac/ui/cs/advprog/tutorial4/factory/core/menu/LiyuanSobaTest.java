package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class LiyuanSobaTest {
    private static String sobaName;
    private static LiyuanSoba liyuanSoba;

    @BeforeAll
    public static void setUp() {
        sobaName = "Yaki Soba";
        liyuanSoba = new LiyuanSoba(sobaName);
    }

    @Test
    public void testSobaHasCorrectIngredients() {
        assertTrue(liyuanSoba.getNoodle() instanceof Soba);
        assertTrue(liyuanSoba.getMeat() instanceof Beef);
        assertTrue(liyuanSoba.getTopping() instanceof Mushroom);
        assertTrue(liyuanSoba.getFlavor() instanceof Sweet);
    }

    @Test
    public void testSobaHasCorrectName() {
        assertEquals(sobaName, liyuanSoba.getName());
    }
}
