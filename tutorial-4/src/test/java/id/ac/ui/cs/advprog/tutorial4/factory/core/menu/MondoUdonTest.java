package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class MondoUdonTest {
    private static String udonName;
    private static MondoUdon mondoUdon;

    @BeforeAll
    public static void setUp() {
        udonName = "Niku Udon";
        mondoUdon = new MondoUdon(udonName);
    }

    @Test
    public void testUdonHasCorrectIngredients() {
        assertTrue(mondoUdon.getNoodle() instanceof Udon);
        assertTrue(mondoUdon.getMeat() instanceof Chicken);
        assertTrue(mondoUdon.getTopping() instanceof Cheese);
        assertTrue(mondoUdon.getFlavor() instanceof Salty);
    }

    @Test
    public void testUdonHasCorrectName() {
        assertEquals(udonName, mondoUdon.getName());
    }
}
