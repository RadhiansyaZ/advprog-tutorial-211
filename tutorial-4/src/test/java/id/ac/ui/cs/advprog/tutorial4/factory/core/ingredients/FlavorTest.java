package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FlavorTest {
    private static List<Flavor> flavorList;

    @BeforeAll
    public static void setUp() {
        flavorList = new ArrayList<>();
        flavorList.add(new Salty());
        flavorList.add(new Spicy());
        flavorList.add(new Sweet());
        flavorList.add(new Umami());
    }

    @Test
    public void testAllFlavorsAreNotAbstract() {
        for(Flavor concrete : flavorList) {
            Class<?> concreteClass = concrete.getClass();

            assertFalse(Modifier.isAbstract(concreteClass.getModifiers()));
        }
    }

    @Test
    public void testAllFlavorsImplementsFlavor() {
        for(Flavor concrete : flavorList) {
            Class<?> concreteClass = concrete.getClass();

            assertTrue(Flavor.class.isAssignableFrom(concreteClass));
        }
    }

    @Test
    public void testAllFlavorsHaveDifferentDescriptions() {
        HashSet<String> flavorDescriptions = new HashSet<>();

        for(Flavor concrete : flavorList) {
            String desc = concrete.getDescription();

            flavorDescriptions.add(desc);
            assertTrue(flavorDescriptions.contains(desc));
        }
    }
}
