package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ToppingTest {
    private static List<Topping> toppingList;

    @BeforeAll
    public static void setUp() {
        toppingList = new ArrayList<>();
        toppingList.add(new BoiledEgg());
        toppingList.add(new Cheese());
        toppingList.add(new Flower());
        toppingList.add(new Mushroom());
    }

    @Test
    public void testAllToppingsAreNotAbstract() {
        for(Topping concrete : toppingList) {
            Class<?> concreteClass = concrete.getClass();

            assertFalse(Modifier.isAbstract(concreteClass.getModifiers()));
        }
    }

    @Test
    public void testAllToppingsImplementsTopping() {
        for(Topping concrete : toppingList) {
            Class<?> concreteClass = concrete.getClass();

            assertTrue(Topping.class.isAssignableFrom(concreteClass));
        }
    }

    @Test
    public void testAllToppingsHaveDifferentDescriptions() {
        HashSet<String> toppingDescriptions = new HashSet<>();

        for(Topping concrete : toppingList) {
            String desc = concrete.getDescription();

            toppingDescriptions.add(desc);
            assertTrue(toppingDescriptions.contains(desc));
        }
    }
}