package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MenuRepositoryTest {
    @Test
    public void testAddAllMenu() {
        MenuRepository menuRepository = new MenuRepository();
        menuRepository.add(new InuzumaRamen("Char Siu Ramen"));
        menuRepository.add(new LiyuanSoba("Yaki Soba"));
        menuRepository.add(new MondoUdon("Moegi Udon"));
        menuRepository.add(new SnevnezhaShirataki("Tartaglia Shirataki"));

        assertEquals(4, menuRepository.getMenus().size());
    }
}
