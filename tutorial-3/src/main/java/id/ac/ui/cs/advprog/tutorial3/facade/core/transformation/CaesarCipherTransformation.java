package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarCipherTransformation {

    public CaesarCipherTransformation(){}

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean isEncode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();

        int shift = isEncode ? 1 : -1;
        int codexSize = codex.getCharSize();
        char[] result = new char[text.length()];

        for(int i = 0; i < text.length(); i++) {
            char nonProcessedChar = text.charAt(i);
            int charIndex = codex.getIndex(nonProcessedChar);

            int newCharIndex = charIndex + shift;
            newCharIndex = newCharIndex < 0 ? codexSize + newCharIndex : newCharIndex % codexSize; // negative index must be added codex size to get positive index
            result[i] = codex.getChar(newCharIndex);
        }

        String processed = new String(result);

        return new Spell(processed, codex);
    }
}
