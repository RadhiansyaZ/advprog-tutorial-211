package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {

    private final QuestRepository questRepository;
    private final Guild guild;
    private final Adventurer agileAdventurer;
    private final Adventurer knightAdventurer;
    private final Adventurer mysticAdventurer;

    public GuildServiceImpl(QuestRepository questRepository) {
        this.questRepository = questRepository;
        this.guild = new Guild();
        //ToDo: Complete Me
        this.agileAdventurer = new AgileAdventurer(this.guild);
        this.knightAdventurer = new KnightAdventurer(this.guild);
        this.mysticAdventurer = new MysticAdventurer(this.guild);
        guild.add(this.agileAdventurer);
        guild.add(this.knightAdventurer);
        guild.add(this.mysticAdventurer);
    }

    //ToDo: Complete Me
    @Override
    public void addQuest(Quest quest) {
        questRepository.save(quest);
        guild.addQuest(quest);
    }

    @Override
    public List<Adventurer> getAdventurers() {
        return guild.getAdventurers();
    }
}
