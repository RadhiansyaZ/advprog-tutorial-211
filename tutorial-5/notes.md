# Requirements
---

## Gambaran Umum
Membuat Sistem Informasi Asisten Dosen. Model yang telah tersedia adalah Mahasiswa dan Mata Kuliah, tetapi belum terdapat model yang menjembatani kedua Model tersebut untuk membuat aplikasi ini fungsional.
## Models
Melihat cerita tersebut, terdapat beberapa models yang bekerja dibalik Sistem Informasi ini, di antaranya : 
1. Mahasiswa, 
2. Mata Kuliah
3. Log

## Relasi Data Models
![ERD](https://files.catbox.moe/4g2r9b.png)

## Endpoints

#### Mata Kuliah

Get All Mata Kuliah (GET) : `http://localhost:8080/mata-kuliah`

Create Mata Kuliah (POST) : `http://localhost:8080/mata-kuliah`
```json
{
	"prodi" :"Ilmu Kapitalisme Pasar", 
	"nama" : "Dasar-Dasar Moda Produksi Kapitalisme 1",
	"kodeMatkul" : "FEB101AS"
}
```
```json
{
    "prodi": "Ilmu Kapitalisme Pasar"
    "nama": "Struktur Buruh dan Aliran Modal Kapitalisme",
    "kodeMatkul": "SBAMK",
}
```

Get Mata Kuliah by Id (GET) : `http://localhost:8080/mata-kuliah/:kodeMatkul`

Update Mata Kuliah (PUT) : `http://localhost:8080/mata-kuliah/:kodeMatkul`
```json
{
	"nama" : "",
	"kodeMatkul" : "",
	"prodi" :"" 
}
```

Delete Mata Kuliah (DELETE) : `http://localhost:8080/mata-kuliah/:kodeMatkul`

#### Mahasiswa

Get all Mahasiswa (GET) : `http://localhost:8080/mahasiswa/`

Create Mahasiswa (POST) : `http://localhost:8080/mahasiswa/`
```json
{
	"nama" : "Ganyu",
	"npm" : "1906398399",
	"email" : "ganyu@ui.ac.id",
	"ipk" : "4",
	"noTelp" : "085730578888"
}
```

Get Mahasiswa by NPM (GET) : `http://localhost:8080/mahasiswa/:npm`

Update Mahasiswa (UPDATE) : `http://localhost:8080/mahasiswa/:npm`
```json
{
	"nama" : "Ganyu",
	"npm" : "1906398399",
	"email" : "ganyu@ui.ac.id",
	"ipk" : "3.5",
	"noTelp" : "085730578888"
}
```

Delete Mahasiswa (DELETE) : `http://localhost:8080/mahasiswa/:npm`

Assign Mahasiswa to Mata Kuliah (POST) : `http://localhost:8080/mahasiswa/:npm/mata-kuliah/:kodeMatkul`

#### Log

Create Log (POST) : `http://localhost:8080/logs/mahasiswa/:npm`
```json
{
	"start" : "2021-03-31T00:00:00",
	"end" : "2021-03-31T04:00:00",
	"deskripsi" : "Tunjangan Bonus Pulsa"
}
```

Get Log by Id (GET) : `http://localhost:8080/logs/:id`

Update Log (PUT) : `http://localhost:8080/logs/:id`
```json
{
	"start" : "2021-03-31T00:00:00",
	"end" : "2021-03-31T03:00:00",
	"deskripsi" : "Tunjangan Bonus Pulsa"
}
```

Delete Log by Id (DELETE) : `http://localhost:8080/log/:id`


Get Logs Report Summary of a Mahasiswa (GET) : `http://localhost:8080/logs/mahasiswa/:npm/reports`