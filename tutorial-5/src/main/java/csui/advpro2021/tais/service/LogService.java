package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;

public interface LogService {
    Log createLog(Log log, String NPM);

    Log getLogById(String idLog);

    Log updateLog(Log log, String idLog);

    void deleteLog(String id);

    Iterable<Log> getListLogsByNPMMahasiswa(String npm, String bulan);

    Iterable<LogSummary> getLogsSummaryByNPMMahasiswa(String npm, String bulan, String tahun);

}
