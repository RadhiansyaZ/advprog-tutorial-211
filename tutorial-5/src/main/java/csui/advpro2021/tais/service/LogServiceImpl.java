package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class LogServiceImpl implements LogService{

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private MahasiswaService mahasiswaService;

    @Override
    public Log createLog(Log log, String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log getLogById(String id) {
        return logRepository.findById(Long.parseLong(id));
    }

    @Override
    public Log updateLog(Log log, String id) {
        Log currentLog = this.getLogById(id);
        log.setMahasiswa(currentLog.getMahasiswa());
        log.setId(Long.parseLong(id));
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLog(String id) {
        logRepository.deleteById(id);
    }

    @Override
    public Iterable<Log> getListLogsByNPMMahasiswa(String npm, String bulan) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        List<Log> logs = logRepository.findByMahasiswa(mahasiswa);
        List<Log> result = new LinkedList<Log>();

        if(bulan != null) {
            for(Log log : logs) {
                if (bulan.equalsIgnoreCase(getMonthName(log.getStart()))) {
                    result.add(log);
                }
            }
        }

        return result;
    }

    @Override
    public Iterable<LogSummary> getLogsSummaryByNPMMahasiswa(String npm, String month, String year) {
        List<LogSummary> summaries = logRepository.getRawLogSummaries(npm);
        List<LogSummary> removed = new ArrayList<>();

        if (month != null) {
            for (LogSummary logSummary : summaries) {
                if (!month.equalsIgnoreCase(logSummary.getBulan())) {
                    removed.add(logSummary);
                }
            }
        }

        if (year != null) {
            for (LogSummary logSummary : summaries) {
                if (Integer.parseInt(year) != logSummary.getTahun()) {
                    removed.add(logSummary);
                }
            }
        }

        for (LogSummary logSummary : removed) {
            summaries.remove(logSummary);
        }

        return summaries;
    }

    protected String getMonthName(LocalDateTime dateTime) {
        Month month = dateTime.getMonth();
        return month.name();
    }

}
