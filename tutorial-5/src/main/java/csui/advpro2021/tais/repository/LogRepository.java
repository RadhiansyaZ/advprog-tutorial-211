package csui.advpro2021.tais.repository;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogRepository extends JpaRepository<Log, String>{
    Log findById(long idLog);

    List<Log> findByMahasiswa(Mahasiswa mahasiswa);

    @Query("SELECT new csui.advpro2021.tais.model.LogSummary( " +
            "EXTRACT (MONTH from log.start) AS bulan, " +
            "EXTRACT (YEAR from log.start) AS tahun, " +
            "SUM(log.durasiKerja))" +
            "FROM Log AS log " +
            "WHERE npm = :npm " +
            "GROUP BY 1, 2")

    List<LogSummary> getRawLogSummaries(String npm);
}
