package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "log")
@Data
@Getter
@NoArgsConstructor
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="id", updatable = false, nullable = false)
    private long id;
    
    @Column(name="deskripsi")
    private String deskripsi;

    @Column(name="start_time")
    private LocalDateTime start;

    @Column(name="end_time")
    private LocalDateTime end;

    @Column(name = "durasi_kerja")
    private double durasiKerja;

    @ManyToOne
    @JoinColumn(name="npm")
    private Mahasiswa mahasiswa;

    @JsonCreator
    public Log(
            @JsonProperty("deskripsi") String deskripsi,
            @JsonProperty("start") String start,
            @JsonProperty("end") String end
    ){
        this.deskripsi = deskripsi;
        this.start = LocalDateTime.parse(start);
        this.end = LocalDateTime.parse(end);
        this.durasiKerja = (this.start.until(this.end, ChronoUnit.SECONDS))/3600.0;
    }

}
