package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    private Mahasiswa mahasiswa;

    private Log log;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        log = new Log();
        log.setStart(LocalDateTime.parse("2021-03-31T00:00:00"));
        log.setEnd(LocalDateTime.parse("2021-03-31T04:00:00"));
        log.setMahasiswa(mahasiswa);
        log.setDeskripsi("Bantuan Pulsa");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String mapLogToJson(Log log) throws JsonProcessingException {
        return String.format("{\"start\": \"2021-03-31T00:00:00\", \"end\": \"2021-03-31T00:00:00\", \"description\": \"%s\"}", log.getDeskripsi());
    }

    @Test
    public void testControllerPostLog() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.createLog(any(Log.class), any(String.class))).thenReturn(log);
        mvc.perform(post("/logs/mahasiswa/1906192052")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapLogToJson(log)))
                .andExpect(jsonPath("$.id").value("0"));
    }

    @Test
    public void testControllerGetLogById() throws Exception {
        when(logService.getLogById("0")).thenReturn(log);
        mvc.perform(get("/logs/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value("0"));
    }

    @Test
    public void testControllerUpdateLog() throws Exception {
        log.setDeskripsi("Deskripsi Baru!");
        when(logService.updateLog(any(Log.class),  anyString())).thenReturn(log);
        mvc.perform(put("/logs/0").contentType(MediaType.APPLICATION_JSON).content(mapLogToJson(log)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("Deskripsi Baru!"));
    }

    @Test
    public void testControllerDeleteLog() throws Exception {
        mvc.perform(delete("/logs/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerGetLogsMahasiswa() throws Exception {
        Iterable<Log> logs = Arrays.asList(log);
        when(logService.getListLogsByNPMMahasiswa("1906192052", null)).thenReturn(logs);

        mvc.perform(get("/logs/mahasiswa/1906192052").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(0));
    }

    @Test
    public void testControllerGetLogsReportMahasiswa() throws Exception {
        Iterable<LogSummary> summaries = Arrays.asList(new LogSummary(3, 2021, 4));
        when(logService.getLogsSummaryByNPMMahasiswa("1906192052", null, null)).thenReturn(summaries);
        mvc.perform(get("/logs/mahasiswa/1906192052/reports").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].bulan").value("March"))
                .andExpect(jsonPath("$[0].tahun").value("2021"));
    }
}
