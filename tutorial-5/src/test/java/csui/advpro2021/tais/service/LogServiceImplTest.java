package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.*;

public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaServiceImpl mahasiswaService;

    @InjectMocks
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;

    private Log log;

    private Log log2;

    private LogSummary logSummary;

    private LogSummary logSummary2;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa(
                "1906192052",
                "Maung Meong",
                "maung@cs.ui.ac.id", "4",
                "081317691718"
        );

        log = new Log();
        log.setStart(LocalDateTime.parse("2021-03-31T00:00:00"));
        log.setEnd(LocalDateTime.parse("2021-03-31T04:00:00"));
        log.setMahasiswa(mahasiswa);
        log.setDeskripsi("Bantuan Pulsa");

        log2 = new Log();
        log2.setStart(LocalDateTime.parse("2020-04-30T00:00:00"));
        log2.setEnd(LocalDateTime.parse("2020-04-30T04:00:00"));
        log2.setMahasiswa(mahasiswa);
        log2.setDeskripsi("Mengoreksi Lab");

        logSummary = new LogSummary(
                log.getStart().getMonth().getValue(),
                log.getStart().getYear(),
                log.getDurasiKerja()
        );

        logSummary2 = new LogSummary(
                log2.getStart().getMonth().getValue(),
                log2.getStart().getYear(),
                log2.getDurasiKerja()
        );

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testServiceCreateLog() {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(logService.createLog(log, mahasiswa.getNpm())).thenReturn(log);
    }

    @Test
    public void testServicegetLogById() {
        lenient().when(logService.getLogById("0")).thenReturn(log);
        Log resultLog = logService.getLogById("0");
        assertEquals(log, resultLog);
    }

    @Test
    public void testServiceUpdateLog() {
        logService.createLog(log, mahasiswa.getNpm());
        String currentLogDescription = log.getDeskripsi();
        log.setDeskripsi("Deskripsi baru!");

        lenient().when(logService.getLogById("0")).thenReturn(log);
        lenient().when(logService.updateLog(log, "0")).thenReturn(log);
        Log resultLog = logService.getLogById("0");

        assertNotEquals(currentLogDescription, resultLog.getDeskripsi());
        assertEquals(log.getDeskripsi(), resultLog.getDeskripsi());
    }

    @Test
    public void testDeleteLog() {
        logService.createLog(log, mahasiswa.getNpm());
        logService.deleteLog("0");
        lenient().when(logService.getLogById("0")).thenReturn(null);
        assertEquals(null, logService.getLogById("0"));
    }

    @Test
    public void testGetListLogsByNpmMahasiswa() {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        Iterable<Log> logs = logRepository.findByMahasiswa(mahasiswa);
        Iterable<Log> result = logService.getListLogsByNPMMahasiswa(mahasiswa.getNpm(), null);
        assertEquals(logs, result);
    }

    @Test
    public void testGetListLogsByNpmMahasiswaWithMonth() {
        List<Log> logs = new LinkedList<Log>(Arrays.asList(log, log2));
        when(logRepository.findByMahasiswa(mahasiswa)).thenReturn(logs);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        Iterable<Log> result = logService.getListLogsByNPMMahasiswa(mahasiswa.getNpm(), "March");
        logs.remove(log2);
        assertEquals(logs, result);
    }

    @Test
    public void testLogsSummaryByNPMMahasiswa() {
        List<LogSummary> summaries = Arrays.asList(logSummary, logSummary2);
        when(logRepository.getRawLogSummaries(mahasiswa.getNpm())).thenReturn(summaries);
        Iterable<LogSummary> summariesResult = logService.getLogsSummaryByNPMMahasiswa(mahasiswa.getNpm(), null, null);
        assertEquals(summaries, summariesResult);
    }

    @Test
    public void testLogsSummaryByNPMMahasiswaWithMonth() {
        List<LogSummary> summaries = new LinkedList<>(Arrays.asList(logSummary, logSummary2));
        when(logRepository.getRawLogSummaries(mahasiswa.getNpm())).thenReturn(summaries);
        Iterable<LogSummary> summariesResult = logService.getLogsSummaryByNPMMahasiswa(mahasiswa.getNpm(), "March", null);
        assertEquals(summaries, summariesResult);
    }

    @Test
    public void testLogsSummaryByNPMMahasiswaWithYear() {
        List<LogSummary> summaries = new LinkedList<>(Arrays.asList(logSummary, logSummary2));
        when(logRepository.getRawLogSummaries(mahasiswa.getNpm())).thenReturn(summaries);
        Iterable<LogSummary> summariesResult = logService.getLogsSummaryByNPMMahasiswa(mahasiswa.getNpm(), null, "2021");
        assertEquals(summaries, summariesResult);
    }

    @Test
    public void testGetMonthName() {
        assertEquals("MARCH", logService.getMonthName(log.getStart()));
    }
}
